-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 28, 2019 at 08:02 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `library`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `gid` int(11) NOT NULL,
  `lid` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `status`, `author`, `gid`, `lid`, `created_at`, `updated_at`) VALUES
(2, 'The Woods', 'Borrowed', 'Ellen Woods', 1, 1, '2019-02-28 06:25:58', '2019-02-27 22:25:58'),
(3, 'Animal Farm', 'Returned', 'George Orwell', 1, 1, '2019-02-28 06:27:21', '2019-02-27 22:27:21'),
(4, 'Harry Potter', 'Returned', 'JK Rowling', 1, 1, '2019-02-28 06:27:34', '2019-02-27 22:27:34'),
(5, 'Cities&Townships', 'Returned', 'Selene DIon', 2, 4, '2019-02-28 06:59:23', '2019-02-27 22:59:23'),
(6, 'SpaceX', 'Returned', 'Elon Musk', 5, 3, '2019-02-28 07:00:01', '2019-02-27 23:00:01');

-- --------------------------------------------------------

--
-- Table structure for table `genre`
--

CREATE TABLE `genre` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `genre`
--

INSERT INTO `genre` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Horror', '2019-02-28 00:00:00', '2019-02-28 06:04:37'),
(2, 'Romance', '2019-02-28 00:00:00', '2019-02-28 06:04:37'),
(3, 'Thriller', '2019-02-28 00:00:00', '2019-02-28 06:04:55'),
(4, 'Fiction', '2019-02-28 00:00:00', '2019-02-28 06:04:55'),
(5, 'NonFiction', '2019-02-28 00:00:00', '2019-02-28 06:05:05');

-- --------------------------------------------------------

--
-- Table structure for table `librarysection`
--

CREATE TABLE `librarysection` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `librarysection`
--

INSERT INTO `librarysection` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Circulation', '2019-02-28 00:00:00', '2019-02-28 06:02:27'),
(2, 'Periodical Section', '2019-02-28 00:00:00', '2019-02-28 06:02:27'),
(3, 'General Reference', '2019-02-28 00:00:00', '2019-02-28 06:03:44'),
(4, 'Children\'s Section', '2019-02-28 00:00:00', '2019-02-28 06:03:44'),
(5, 'Fiction', '2019-02-28 00:00:00', '2019-02-28 06:04:08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gid` (`gid`),
  ADD KEY `lid` (`lid`);

--
-- Indexes for table `genre`
--
ALTER TABLE `genre`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `librarysection`
--
ALTER TABLE `librarysection`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `genre`
--
ALTER TABLE `genre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `librarysection`
--
ALTER TABLE `librarysection`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `books_ibfk_1` FOREIGN KEY (`gid`) REFERENCES `genre` (`id`),
  ADD CONSTRAINT `books_ibfk_2` FOREIGN KEY (`lid`) REFERENCES `librarysection` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
