<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{

protected $table = 'genre';
public $timestamps = true;

  public function books()
{
    return $this->hasMany('App\Books', 'gid');
}

}
