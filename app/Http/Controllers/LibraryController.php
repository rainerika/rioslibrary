<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;



use Exception;
use Storage;
use Image;
use DB;
use Redirect;
use Session;

use App\Books;
use App\LibrarySection;
use App\Genre;

class  LibraryController extends Controller
{

	public function __construct()
	{


	}



	public function books()
	{
		$table_data= Books::get();
		return view('table',compact('table_data'));
	}
	public function add()
	{

		$librarysection= LibrarySection::get();
		$genre= Genre::get();

		return view('add',compact('librarysection','genre'));
	}
	public function addBook(Request $request)
	{

		$book= new Books;

		$book->title=$request->title;
		$book->author=$request->author;
		$book->status="Returned";
		$book->lid=$request->librarysection;
		$book->gid=$request->genre;
		$book->save();

		if($book->save()){
			return Redirect()->route('books');
		}



	}

	public function returnBook(Request $request, $id)
	{
    //
		if($request->ajax()){

			$book = new Books;
			$data = $book->find($id);
			$data->status = 1;


			if($data->save()){
				return response(['status'=>'success']);
			}
			else{
				return response(['status'=>'failed']);
			}

		}

	}
	public function borrowBook(Request $request, $id)
	{
    //
		if($request->ajax()){

			$book = new Books;
			$data = $book->find($id);
			$data->status = 0;

			if($data->save()){
				return response(['status'=>'success']);
			}
			else{
				return response(['status'=>'failed']);
			}

		}

	}


}