<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibrarySection extends Model
{

protected $table = 'librarysection';
public $timestamps = true;

  public function books()
{
    return $this->hasMany('App\Books', 'lid');
}

}
