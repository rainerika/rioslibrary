<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Books extends Model
{

protected $table = 'books';
public $timestamps = true;

   public function librarysection()
{
    return $this->belongsTo('App\LibrarySection', 'lid');
}

   public function genre()
{
    return $this->belongsTo('App\Genre', 'gid');
}


}
