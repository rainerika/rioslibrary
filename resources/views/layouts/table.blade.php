<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Rio's Library Catalogue</title>

  <!-- Custom fonts for this template-->
  {!! Html::style( 'vendor/fontawesome-free/css/all.min.css') !!}
  <!-- Page level plugin CSS-->
  {!! Html::style( 'vendor/datatables/dataTables.bootstrap4.css') !!}

  <!-- Custom styles for this template-->
  {!! Html::style( 'css/sb-admin.css') !!}

  <!-- Bootstrap core JavaScript-->
  {!! Html::script('vendor/jquery/jquery.min.js') !!}
  {!! Html::script('vendor/bootstrap/js/bootstrap.bundle.min.js') !!}

  <!-- Core plugin JavaScript-->
  {!! Html::script('vendor/jquery-easing/jquery.easing.min.js') !!}
  <!-- Page level plugin JavaScript-->
  {!! Html::script('vendor/datatables/jquery.dataTables.js') !!}
  {!! Html::script('vendor/datatables/dataTables.bootstrap4.js') !!}

  <!-- Custom scripts for all pages-->
  {!! Html::script('js/sb-admin.min.js') !!}

  <!-- Demo scripts for this page-->
  {!! Html::script('js/demo/datatables-demo.js') !!}


  
</head>

<body id="page-top">



  @yield('content')

  <!-- Sticky Footer -->
  <!--
  <footer class="sticky-footer">
    <div class="container my-auto">
      <div class="copyright text-center my-auto">
        <span>Copyright © Your Website 2019</span>
      </div>
    </div>
  </footer>
-->
    <!-- modal -->

</body>
</html>




