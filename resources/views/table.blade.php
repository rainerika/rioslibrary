@extends('layouts.table')
@section('content')

<div id="wrapper">




  <div id="content-wrapper">

    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
         Rio's Library
       </li>
       <li class="breadcrumb-item active">Catalogue</li>
     </ol>


     @if(Session::has('message'))
     <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
     @endif

     <div class="card mb-3">
      <div class="card-header">
        <i class="fas fa-plus"></i>
        <a href="{{ route('add') }}"> Add Book </a> </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTableID" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Title</th>
                  <th>Author</th>
                  <th>Genre</th>
                  <th>Library Section</th>
                  <th> Status </th>

                </tr>
              </thead>

              <tbody>
               @if(count($table_data) > 0)
               @foreach($table_data as $row)
               <tr class="{{$row->id}}">
                <td>{{$row->title}}</td>
                <td>{{$row->author}}</td>
                <td>{{$row->genre->name}}</td>
                <td> {{$row->librarysection->name}}</td>
                <td> {{$row->status}}</td>
              </tr>

              @endforeach
              @else
              <tr><td colspan="5"><p class="text-center">No Data Found<p></td></tr>
                @endif  


              </tbody>
            </table>
          </div>
        </div>




      </div>
      <!-- /.container-fluid -->

    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->




  <script>
  $(document).ready( function(){
    var currentUrl = window.location;

    $('#dataTableID').dataTable( {
      responsive: true,
      searching: true,
      pageLength: 100,
    });

  });

  </script>

  @endsection