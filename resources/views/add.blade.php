@extends('layouts.table')
@section('content')

<div id="wrapper">




  <div id="content-wrapper">

    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{ route('books') }}">Rio's Library</a>
        </li>
        <li class="breadcrumb-item active">Add Book</li>
      </ol>

      <br>
      @if(Session::has('message'))
      <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
      @endif

      {!! Form::open(['route' => 'book.store',  'files'=> true, 'class'=>'form', 'id'=>'ad-content']) !!}

      <div class="form-group col-sm-8">
        <label for="content_title">Title</label>
        {!! Form::text('title', null, ['placeholder'=>'Enter Title', 'class'=>'form-control']) !!}

      </div>
      <br>
      <div class="form-group col-sm-8">
        <label for="content_title">Author/s</label>
        {!! Form::text('author', null, ['placeholder'=>'Enter Author/s', 'class'=>'form-control']) !!}

      </div>

      <div class="form-group col-sm-8">
        <label for="content_title">Library Section</label>
        <select name="librarysection" class="form-control">
         @foreach($librarysection as $row)
         
         <option value="{{$row->id}}">{{ucwords($row->name)}}</option>
         @endforeach
       </select>
     </div>
     <br>

     <div class="form-group col-sm-8">
      <label for="content_title">Genre</label>
      <select name="genre" class="form-control">
       @foreach($genre as $row)
       
       <option value="{{$row->id}}">{{ucwords($row->name)}}</option>
       @endforeach
     </select>

   </div>





   <br/>
   <div class="form-group col-lg-8">     
    {!! Form::submit('Save', ['class'=>'btn btn-lg btn-primary pull-right']) !!} 
  </div>
  {!! Form::close() !!}





</div>
<!-- /.container-fluid -->

</div>
<!-- /.content-wrapper -->

</div>
<!-- /#wrapper -->





@endsection