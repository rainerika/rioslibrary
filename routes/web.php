<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('books', ['as'=>'books', 'uses' => 'LibraryController@books']);
Route::get('add', ['as'=>'add', 'uses' => 'LibraryController@add']);
Route::post('book/add', ['as'=>'book.store', 'uses' => 'LibraryController@addBook']);
Route::post('book/return/{id}', ['as'=>'book.return', 'uses' => 'LibraryController@returnBook']);
Route::post('book/borrow/{id}', ['as'=>'book.borrow', 'uses' => 'LibraryController@borrowBook']);